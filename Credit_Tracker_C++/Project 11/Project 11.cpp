// Project 11.cpp : keeps track of one Customer’s credit card information.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

//the cutomer class
class Customer {
public:   // The class' public functions
	void setAcct(int aNo);
	void setCredit(double amount);  //precondition: amount is positive

	int getAcctNo() const;
	double getCreditLimit() const;
	double getBal() const;

	double availCredit() const;
	void moreCredit(double amount); // precondition: amount is positive
	bool makePurchase(double amount);  // precondition: amount is positive
	void display() const;
	void makePayment(double amount); // precondition: amount is positive

private:  // The class' private internal data members
	int accountNo;//customer’s account number
	double creditLimit;// max credit available
	double balance = 0.0;// current balance – new customer balance is 0.0

};

//sets cutomers account number (accountNo)
void Customer::setAcct(int aNo)
{
	accountNo = aNo;
	return;
}
//sets customers credit limit
void Customer::setCredit(double amount)
{
	creditLimit = amount;
	return;
}
//retreives the customers account number
int Customer::getAcctNo() const
{
	return accountNo;
}
//retreives the customers credit limit
double Customer::getCreditLimit() const
{
	return creditLimit;
}
//retreives the customers remaining balance
double Customer::getBal() const
{
	return balance;
}
// checks the availible credit
double Customer::availCredit() const
{
	return (creditLimit - balance);
}
//gives customer more credit
void Customer::moreCredit(double amount)
{
	creditLimit = creditLimit + amount;
	return;
}
//customer makes a payment
void Customer::makePayment(double amount)
{
	balance = balance - amount;
	return;
}
//gives customer makes a purchase
bool Customer::makePurchase(double amount)
{
	if (amount <= availCredit())
	{
		balance = balance + amount;
		return true;
	}
	else
	{
		return false;
	}
}
//displays cutomer data
void Customer::display() const
{
	cout << "customer with Account #: " << accountNo << " has a maximum credit limit of " << creditLimit << "$ and had a current balance of " << balance << "$" << endl;
	return;
}
int main()
{
	//create a customer object
	Customer newCustomer;
	//enter a valid number for the account
	string accountNum = "";
	int num1 = 0;
	while (true) {
		cout << "enter an account number for the new customer: ";
		getline(std::cin, accountNum);
		stringstream myStream(accountNum);
		if (myStream >> num1)
			break;
		cout << "Invalid, please try again" << endl;
	}
	newCustomer.setAcct(num1);
	//enter a valid number for the account
	string creditLimit = "";
	double num2 = 0.0;
	while (true) {
		cout << "enter a credit limit for the new customer: ";
		getline(std::cin, creditLimit);
		stringstream myStream(creditLimit);
		if (myStream >> num2)
			break;
		cout << "Invalid, please try again" << endl;
	}
	newCustomer.setCredit(num2);
	//display the customers info using the get functions
	cout << "customer with Account #: " << newCustomer.getAcctNo() << " has a maximum credit limit of " << newCustomer.getCreditLimit() << "$ and had a current balance of " << newCustomer.getBal() << "$" << endl;
	//ask how many months will be simulated
	string userMonths = "";
	int numMonths = 0;
	while (true) {
		cout << "enter a number of months to simulate: ";
		getline(std::cin, userMonths);
		stringstream myStream(userMonths);
		if (myStream >> numMonths)
			break;
		cout << "Invalid, please try again" << endl;
	}
	//creat a for loop executing the user specified number of months
	for (int x = 1; x <= numMonths;x++)
	{
		//Ask the user to provide all total of all purchases for the customer
		string totalPurchases = "";
		int purchaseAmount = 0;
		while (true) {
			cout << "enter the cutomers total purchases for the month: ";
			getline(std::cin, totalPurchases);
			stringstream myStream(totalPurchases);
			if (myStream >> purchaseAmount)
				break;
			cout << "Invalid, please try again" << endl;
		}
		if (newCustomer.makePurchase(purchaseAmount))
		{
			cout << "purchases were successful" << endl;
		}
		else
		{
			cout << "purchases were unsuccessful due to insufficient credit limit" << endl;
		}
		// Ask the user to provide a payment for the customer
		string totalPayment = "";
		int paymentAmount = 0;
		while (true) {
			cout << "enter the cutomers total payments for the month: ";
			getline(std::cin, totalPayment);
			stringstream myStream(totalPayment);
			if (myStream >> paymentAmount)
				break;
			cout << "Invalid, please try again" << endl;
		}
		newCustomer.makePayment(paymentAmount);
		//Output the current balance for the customer
		cout << "After the above actions the customers current balance is :" << newCustomer.getBal() << "$" << endl;
		//Display the Customer’s info using the display function
		newCustomer.display();


		cout << "the number of months remaining in the simulation are: " << (numMonths - x) << endl;
	}
	int temp = 0;
	cout << "enter anything and press enter to continue: ";
	cin >> temp;
	return 0;
}